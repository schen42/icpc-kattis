#include <iostream>
using namespace std;
/*
  Naive implementation: Take a binary digit one at a time (by using bit shifts) and OR 
  it into the correct place.  The twist is that we have to ignore the padding.
*/
int main()
{
  unsigned int input;
  cin >> input;

  unsigned int bit;
  unsigned int reversed = 0;

  int i = 0;
  while ((input & 0x80000000) == 0)
  {
    ++i;
    input <<= 1;
  }
  unsigned int rem = 32 - i;
  for (int j = 0; j < rem; ++j)
  {
    bit = (input & 0x80000000) >> 31;
    reversed |= (bit << j);
    input <<= 1;
  }
  cout << reversed << endl; 
}
