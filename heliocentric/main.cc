#include <iostream>
using namespace std;

void increment_days(unsigned int& days, unsigned int turnover)
{
  if (++days >= turnover)
  {
    days = 0;
  }
}

void increment_earth_days(unsigned int& days)
{
  increment_days(days, 365);
}

void increment_mars_days(unsigned int& days)
{
  increment_days(days, 687);
}

int main()
{
  unsigned int earth_start, mars_start;
  unsigned int case_num = 1;
  unsigned int answer;
  while (cin >> earth_start >> mars_start)
  {
    answer = 0;
    while (earth_start != 0 || mars_start != 0)
    {
      increment_earth_days(earth_start);
      increment_mars_days(mars_start);
      answer += 1;
    }
    printf("Case %d: %d\n", case_num++, answer);
  }

  return 0;
}
