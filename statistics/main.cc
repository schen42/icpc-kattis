#include <iostream>
using namespace std;
int main()
{
  unsigned int num_samples;
  int case_num = 1;
  int sample;
  int case_min, case_max, case_range;
  while (cin >> num_samples)
  {
    case_min = 1000000; case_max = -1000000; case_range = 0;
    for (int i = 0; i < num_samples; ++i)
    {
      cin >> sample;
      if (sample < case_min)
      {
        case_min = sample;
      }
      if (sample > case_max)
      {
        case_max = sample;
      }
    }
    printf("Case %d: %d %d %d\n", case_num, case_min, case_max, case_max - case_min);
    ++case_num;
  }
  return 0;
}
